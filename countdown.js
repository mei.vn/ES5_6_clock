var timer = [];
class CountDown {
	constructor(id = 0, seconds = 60) {
		this.seconds = seconds;
		this.id = id;
	}
	getSeconds() {
		return this.seconds;
	}
	getId() {
		return this.id;
	}
	setSeconds(seconds) {
		this.seconds = seconds;
	}
	decodeTime() {
		var time = [],
			tmp = this.seconds,
			remain = 0;
		time['hours'] = this.getFull(Math.floor(tmp / 3600));
		tmp -= time['hours'] * 3600;

		time['minutes'] = this.getFull(Math.floor(tmp / 60));
		tmp -= time['minutes'] * 60;

		time['seconds'] = this.getFull(parseInt(tmp % 60, 10));
		return time;
	}
	getFull(time) {
		if (time < 10) {
			return '0' + time;
		}
		return time;
	}
	start(callback) {
		// var cr_time = parseInt(countdown.getSeconds());
		timer[this.id] = setInterval(
			function() {
				this.seconds -= 1;
				var res = this.decodeTime();
				console.log(`Countdown ${this.id} - ` + res['hours'] + ':' + res['minutes'] + ':' + res['seconds']);
				if (this.seconds <= 0) {
					callback();
					clearInterval(timer[this.id]);
					return true;
				}
			}.bind(this),
			1000
		);
	}
	delete() {
		window.clearInterval(timer[this.id]);
	}
}
